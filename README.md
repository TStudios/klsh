# klsh
KLSH is a shell created for programers, for fun and more. Its built ontop of bash and most bash things work with it


# Installing
Simply download the Installer file: [GitHub](https://github.com/codingbunnys/klsh/releases) or [GitLab](https://gitlab.com/TStudios/klsh/-/releases) (Downloads for any version higher then or equal to 1.0-alpha.2), give it execute permitions (in a terminal:`chmod 755 [FILEPATH]`) and then run it!

# Releases pages
[GitHub](https://github.com/codingbunnys/klsh/releases)
[GitLab](https://gitlab.com/TStudios/klsh/-/releases)

# Using it
Open a bash terminal and type `klsh`

# Commands
Coming soon... Once the list mostly done it will be available [here!](https://Github.com/codingbunnys/klsh/wiki) (Comunity Contributions Accepted!)

# Using it in scripts
## For scripts it's required in:
Add
``if [[ -f $HOME/.klsh/.klshcore ]];then source $HOME/.klsh/.klshcore -s;else echo -e -n "ERROR:\n   KLSH IS REQUIRED. Do you wish to install it? (Y/n - Anything exept a lowercase n will trigger it to install) ";read -rsn1 key;if [[ "$key" != "n" ]];then echo "";pwd=$PWD;cd ~;mkdir UPDATER;cd UPDATER;wget --quiet https://raw.githubusercontent.com/codingbunnys/klsh/master/version;laver=`cat version`;wget --quiet https://raw.githubusercontent.com/codingbunnys/klsh/master/installerfiles/versions/$laver/installer;chmod 755 installer;./installer;cd ..;rm -rf UPDATER;cd $pwd;else echo -e "\nKLSH Is not gonna be installed. Exiting!";exit 127;fi;fi``
to the begining of your script  
[Unminified Version](https://gitlab.com/TStudios/klsh/raw/master/dev-presets/Required)  
[Minifier tool](https://bash-minifier.appspot.com/)   
## For scripts who have klsh as an optional lib/extention/thing
Add
``if [[ -f "$HOME/.klsh/.klshcore" ]]; then source $HOME/.klsh/.klshcore -s; fi``
to the begining of your script  
[Unminified Version](https://gitlab.com/TStudios/klsh/raw/master/dev-presets/NotRequired)  
[Minifier tool](https://bash-minifier.appspot.com/)  
See [dev-presets/README.md](https://gitlab.com/TStudios/klsh/blob/master/dev-presets/README.md) (if there are updates ill only update this there)
