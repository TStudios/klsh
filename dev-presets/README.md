# About this folder
This is for if you want to use KLSH for your bash projects! Files in here are unminified versions of the code below

# Minified Code Examples (also in the README.md in the parent directory)
## For scripts it's required in:
Add
``if [[ -f $HOME/.klsh/.klshcore ]];then source $HOME/.klsh/.klshcore -s;else echo -e -n "ERROR:\n   KLSH IS REQUIRED. Do you wish to install it? (Y/n - Anything exept a lowercase n will trigger it to install) ";read -rsn1 key;if [[ "$key" != "n" ]];then echo "";pwd=$PWD;cd ~;mkdir UPDATER;cd UPDATER;wget --quiet https://raw.githubusercontent.com/codingbunnys/klsh/master/version;laver=`cat version`;wget --quiet https://raw.githubusercontent.com/codingbunnys/klsh/master/installerfiles/versions/$laver/installer;chmod 755 installer;./installer;cd ..;rm -rf UPDATER;cd $pwd;else echo -e "\nKLSH Is not gonna be installed. Exiting!";exit 127;fi;fi``
to the begining of your script  
[Unminified Version](https://gitlab.com/TStudios/klsh/raw/master/dev-presets/Required)  
[Minifier tool](https://bash-minifier.appspot.com/)  
## For scripts who have klsh as an optional lib/extention/thing
Add
``if [[ -f "$HOME/.klsh/.klshcore" ]]; then source $HOME/.klsh/.klshcore -s; fi``
to the begining of your script  
[Unminified Version](https://gitlab.com/TStudios/klsh/raw/master/dev-presets/NotRequired)  
[Minifier tool](https://bash-minifier.appspot.com/)  
